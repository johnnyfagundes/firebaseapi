const functions = require('firebase-functions');

const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);

exports.onTaskCreate = functions
    .database
    .ref('tasks/{taskId}')
    .onCreate((snapshot, context) => {

        const json = snapshot.val();
        const key = context.params.taskId;
        const newObj = {
            createAt: context.timestamp
        }
        const log = Object.assign(newObj, json);

        return admin
            .database()
            .ref(`/logs/${key}`)
            .set(log);
        
    });

exports.onTaskDelete = functions
    .database
    .ref('tasks/{taskId}')
    .onDelete((snapshot, context) => {

        const json = snapshot.val();
        const key = context.params.taskId;
        const newObj = {
            deletedAt: context.timestamp
        }
        const log = Object.assign(newObj, json);

        return admin
            .database()
            .ref(`/logs/${key}`)
            .set(log);
        
    });

exports.onTaskUpdate = functions
    .database
    .ref('/tasks/{taskId}')
    .onUpdate((change, context) => {

        const json = change.after.val();
        const key = context.params.taskId;
        const newObj = {
            alteredAt: context.timestamp
        }
        const log = Object.assign(newObj, json);

        return admin
            .database()
            .ref(`/logs/${key}`)
            .set(log);
    });

exports.onTaskWrite = functions
    .database
    .ref('/tasks/{taskId}')
    .onWrite((change, context) => {
        
        if (change.before.exists()) {
            return null;
        }
        
        if (!change.after.exists()) {
            return null;
        }

        const key = context.params.taskId;
        const json = change.after.val();
        const uppercase = json.toUpperCase();

        return admin
            .database()
            .ref(`/tasks/${key}`)
            .set(uppercase);
    });
